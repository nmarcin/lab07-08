package pl.edu.pwsztar.domain.dto;

import java.util.List;

public class FileDto {

    private final List<MovieDto> movieDtoList;

    private FileDto(Builder builder) {
        this.movieDtoList = builder.movieDtoList;
    }

    public static Builder Builder() {
        return new Builder();
    }

    public List<MovieDto> getMovieDtoList() {
        return movieDtoList;
    }

    public static final class Builder {
        private List<MovieDto> movieDtoList;

        private Builder() {}

        public Builder movies(List<MovieDto> movieDtoList) {
            this.movieDtoList = movieDtoList;

            return this;
        }

        public FileDto build() {
            return new FileDto(this);
        }
    }
}

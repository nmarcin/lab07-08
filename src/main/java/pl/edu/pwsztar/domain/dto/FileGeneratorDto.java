package pl.edu.pwsztar.domain.dto;

import org.springframework.core.io.InputStreamResource;

public class FileGeneratorDto {

    private InputStreamResource inputStreamResource;
    private Long length;
    private String fileName;

    public FileGeneratorDto(Builder builder) {
        inputStreamResource = builder.inputStreamResource;
        length = builder.length;
        fileName = builder.fileName;
    }

    public InputStreamResource getInputStreamResource() {
        return inputStreamResource;
    }

    public String getFileName() {
        return fileName;
    }

    public Long getLength() {
        return length;
    }

    public static final class Builder {
        private InputStreamResource inputStreamResource;
        private Long length;
        private String fileName;

        public Builder() {}

        public Builder inputStreamResource(InputStreamResource inputStreamResource) {
            this.inputStreamResource = inputStreamResource;

            return this;
        }

        public Builder length(Long length) {
            this.length = length;

            return this;
        }

        public Builder fileName(String fileName) {
            this.fileName = fileName;

            return this;
        }

        public FileGeneratorDto build() {
            return new FileGeneratorDto(this);
        }
    }
}

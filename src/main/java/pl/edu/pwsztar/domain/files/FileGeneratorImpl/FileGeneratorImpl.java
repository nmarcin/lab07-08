package pl.edu.pwsztar.domain.files.FileGeneratorImpl;

import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.dto.FileGeneratorDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.files.FileGenerator;

import java.io.*;
import java.util.Date;

@Component
public class FileGeneratorImpl implements FileGenerator {

    @Override
    public FileGeneratorDto toTxt(FileDto fileDto) throws IOException {
        File file = saveToFile(fileDto);

        InputStream inputstream = new FileInputStream(file);
        InputStreamResource inputStreamResource = new InputStreamResource(inputstream);

        return new FileGeneratorDto.Builder()
                .inputStreamResource(inputStreamResource)
                .length(file.length())
                .fileName("test_" + new Date().getTime() + ".txt")
                .build();
    }

    private File saveToFile(FileDto fileDto) throws IOException {
        File file = File.createTempFile("tmp", ".txt");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));

        for (MovieDto movieDto : fileDto.getMovieDtoList()) {
            bufferedWriter.write(movieDto.getYear() + " " + movieDto.getTitle());
            bufferedWriter.newLine();
        }

        bufferedWriter.close();
        fileOutputStream.flush();
        fileOutputStream.close();

        return file;
    }
}
